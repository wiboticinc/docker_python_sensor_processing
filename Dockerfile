FROM python:3.7.2-alpine as pipbuild

RUN \
  apk --no-cache add musl-dev g++ && \
  pip install --install-option="--prefix=/pipinstall" numpy

# Use a multi-stage build to not include build tools in image
FROM python:3.7.2-alpine
COPY --from=pipbuild /pipinstall /usr/local

